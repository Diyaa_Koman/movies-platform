@extends('layouts.app')

@section('content')


<div class="product-section container">


        <div class="product-section-image">

            <img src="{{asset('storage/'.$movie->image)}}" alt="movie">
        </div>

        <div class="product-section-information">
            <h1 class="product-section-title">{{ $movie->name }}</h1>

            <div>
                @foreach($categories as $category)
                    <h3 style="color: green">{{$category->name}}</h3>
                @endforeach
            </div>
            <p class="border p-2">
                {!! $movie->description !!}
            </p>

            <p>&nbsp;</p>



        </div>




</div> <!-- end product-section -->

@endsection
