<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Movies Platform</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link rel="stylesheet" href="{{asset('css/temp.css')}}" type="text/css" media="all" />

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .checked{
            color: orange;
        }
    </style>
</head>
<body>
<!-- START PAGE SOURCE -->
<div id="shell">
    <div id="header">

        @auth
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
                <div class="right p-2">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Logout') }}
                    </button>
                </div>

            </form>
        @else
            <div class="right p-2">
                <a href="{{route('register')}}" class="btn btn-primary">
                    {{ __('Register') }}
                </a>
            </div>
            <div class="right p-2">
                <a href="{{route('login')}}" class="btn btn-primary">
                    {{ __('Login') }}
                </a>
            </div>
        @endauth

        <h1 class="" style="margin: 50px ; font-size: 50px ; color: red">Movies Platform</h1>

        <div id="sub-navigation">
            <ul>
                <li><a href="{{route('home')}}">SHOW ALL</a></li>
                <li><a href="#">LATEST TRAILERS</a></li>
                <li><a href="#">TOP RATED</a></li>
                <li><a href="#">MOST COMMENTED</a></li>
            </ul>
            <div id="search">
                <form action="{{route('home')}}" method="get" accept-charset="utf-8">
                    <label for="search-field">SEARCH</label>
                    <input type="text" name="search_field"  placeholder="Enter search here" id="search-field" class="blink search-field"  />
                    <input type="submit" value="GO!" class="search-button" />
                </form>
            </div>
        </div>
    </div>
    <div id="main">
        <div id="content" class="py-4">

            @yield('content')

        </div>
    </div>

    <div class="cl">&nbsp;</div>
</div>
<div id="footer mt-5">
    <p class="lf">Copyright &copy; 2021 <a href="#">Movies Platform</a> - All Rights Reserved</p>
    <p class="rf">Design by <a href="http://diyaa.com/">Diyaa</a></p>
    <div style="clear:both;"></div>
</div>
</div>
<!-- END PAGE SOURCE -->
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('js/jquery-1.4.2.min.js')}}"></script>
</html>

