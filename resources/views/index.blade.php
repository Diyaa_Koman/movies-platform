@extends('layouts.app')

@section('content')

    <div class="box">
        <div class="head">
        </div>
        <div class="row p-3">
            @if($categories_select)
                <h5>Categories Selected:</h5>
            @foreach($categories_select as $category_select)
                <h3>{{$category_select->name}}</h3>
            @endforeach
            @endif
        </div>
        @forelse($movies as $movie)

            <div class="movie m-2">
                <div class="movie-image">
                    <span class="play"><span class="name">{{$movie->name}}</span></span>
                    <a href="{{route('movie' , $movie->id)}}"><img src="{{asset('storage/'.$movie->image)}}" alt="" /></a>
                </div>
                <h>{{$movie->name}}</h>
                <div class="rating">
                    <p style="margin: 5px;">RATING</p>
                    <div class="" >
                        @for($rate = 1 ; $rate<=5 ; $rate++)
                            <span class="fa fa-star @if($movie->rate >= $rate)checked @endif"></span>
                        @endfor
                    </div>
                </div>
            </div>

        @empty
            <div style="text-align : left">No items found</div>

        @endforelse

        <div class="sidebar">
            <div class="widget">
                <h3 class="widgettitle">Categories</h3>
                <form method="post" id="framework_form" action="{{ route('store') }}">
                    @csrf
                    <ul>
                        <select id="framework" name="category[]" multiple class="form-control" >
                            @foreach($categories as $key => $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <div class="form-group">
                            <input type="submit" class="btn btn-info" name="submit" value="Filter" />
                        </div>
                    </ul>
                </form>
            </div>
        </div>

        <div class="cl" style="margin-bottom: 200px">&nbsp;</div>
    </div>


@endsection
