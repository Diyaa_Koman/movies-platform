<?php

use App\Models\Order;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

////////////////////////////////////////////// Auth ///////////////////////////////////////////////


Route::post('register' , 'AuthController@register')->name('register');
Route::post('login' , 'AuthController@login')->name('login');


Route::group(['middleware' => ['auth:user-api']] , function (){
    Route::post('logout' , 'AuthController@logout')->name('logout');
    Route::get('profile' , 'AuthController@profile');
});


////////////////////////////////////////////// Category ///////////////////////////////////////////////


Route::group(['middleware' => ['auth:user-api'] , 'prefix' => 'category'] , function (){
    Route::get('get-all' , 'CategoryController@getAll');
    Route::get('get-one' , 'CategoryController@getOne');
    Route::post('create' , 'CategoryController@create');
    Route::put('update' , 'CategoryController@update');
    Route::delete('delete' , 'CategoryController@delete');
});


////////////////////////////////////////////// Movie ///////////////////////////////////////////////


Route::group(['middleware' => ['auth:user-api'] , 'prefix' => 'movie'] , function (){
    Route::get('get-all' , 'MovieController@getAll');
    Route::get('get-one' , 'MovieController@getOne');
    Route::post('create' , 'MovieController@create');
    Route::post('update' , 'MovieController@update');
    Route::delete('delete' , 'MovieController@delete');
    Route::post('create-associate' , 'MovieController@create_associate');
    Route::delete('delete-associate' , 'MovieController@delete_associate');
});





