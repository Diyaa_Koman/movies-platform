<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/store', 'HomeController@index')->name('store');

Route::get('/show-movie/{id}', 'HomeController@show_movie')->name('movie');
