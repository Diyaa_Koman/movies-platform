<?php


use App\enums\ErrorCode;
use App\Models\API\other\ApiDataResult;
use App\Models\API\other\ApiMessage;
use App\Models\API\other\ApiResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;

function getCurrentLang()
{
    return app()->getLocale();
}

function returnError($msg, $ex = '' , $errNum = \App\enums\ErrorCode::error )
{
    if($ex !== '')
        return response()->json(api_error_msg($msg , $errNum , 'Error' , $ex))
            ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

    return response()->json(api_error_msg($msg , $errNum , 'Error' , $ex))
        ->setStatusCode(Response::HTTP_BAD_REQUEST);
}

function returnSuccess($msg , $ex = '' , $errNum = \App\enums\ErrorCode::success )
{
    return response()->json(api_error_msg($msg , $errNum , 'Success' , $ex))
        ->setStatusCode(Response::HTTP_ACCEPTED);
}

function stopv($obj = '', $msg = '') {
    echo $msg ? "$msg<br/>" : "";
    var_dump($obj);
    die();
}

function api_error_msg($message, $code , $type , $ex) {
    if(!$code){
        $code = \App\enums\ErrorCode::error;
    }
    $model = new ApiResult([
        'isOk' => $type == 'Success',
        'message' => new ApiMessage([
            'type' => $type,
            'code' => (int)$code,
            'content' => is_string($message) ? array($message) : $message,
        ]),
        'exception' => $ex
    ]);
    return $model;
}

function user() {
    return Auth::guard('user-api')->user();
}

function admin() {
    return Auth::guard('admin-api')->user();
}

function uploadImage($image , $fileName)
{
    $imageName =  time().'.'. $image->extension();
    $imageToSave = $fileName . '/' . time().'.'. $image->extension();

    $image->move(public_path('storage'. DIRECTORY_SEPARATOR .$fileName), $imageName);
    return $imageToSave;
}

function getImage($image)
{
    return URL::to('/') . '/storage/' . $image;
}

function setActiveCategory($category , $output = "background-color: #f4f4f4;")
{
    if(request()->category)
    foreach(request()->category as $cat) {
        if($cat == $category)
            return $output;
    }
    return  '';
}
