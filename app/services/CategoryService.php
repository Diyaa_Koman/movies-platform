<?php

namespace App\services;

use App\enums\ActiveInactiveStatus;
use App\enums\ErrorCode;
use App\enums\OfferStatus;
use App\enums\RequestStatus;
use App\enums\UserStatus;
use App\Http\Requests\OfferRequest;
use App\Models\API\lists\CarTypeResult;
use App\Models\API\orders\OfferResult;
use App\Models\API\orders\OrderResult;
use App\Models\API\other\ApiMessage;
use App\Models\CarType;
use App\Models\Category;
use App\Models\Location;
use App\Models\notifications\Notification;
use App\Models\notifications\offer\DeleteOfferNotification;
use App\Models\notifications\offer\NewOfferNotification;
use App\Models\notifications\order\NewOrderNotification;
use App\Models\Offer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Queue\RedisQueue;


class CategoryService {

    public static function getAll() {

        try {

            $query = Category::get();

            $data = [];
            foreach ($query as $one) {

                $item = FillApiModelService::FillCategoryApiModel($one);
                $data[] = $item;
            }

            return [true , FillApiModelService::FillCategoryResultApi($data) , '' , ''];
        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }
    }


    public static function getOne($request) {

        try {

            $category = Category::find($request->category_id);

            if(!$category){
                return [false , null , "This category is not found !" , ''];
            }

            $data = FillApiModelService::FillCategoryApiModel($category);

            return [true , FillApiModelService::FillCategoryResultApi($data) , '' , ''];
        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }
    }

    public static function createUpdateCategory($request){
        try{

            $category= (isset($request->category_id)) ? Category::find($request->category_id) : new Category();
            $category->name = isset($request->name) ? $request->name : $category->name;
            $category->status = isset($request->status) ? $request->status : ActiveInactiveStatus::active;

            $category->save();
            $data = FillApiModelService::FillCategoryApiModel($category);
            $data = FillApiModelService::FillCategoryResultApi($data);
            return [true , $data , '' , ''];

        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }

    }



    public static function delete($request){

        try {
            $category = Category::find($request->category_id);

            if (!$category)
                return returnError("This category is not found !");

            $category->delete();
            return returnSuccess("This category has been deleted");
        }catch (\Exception $ex){
            return returnError(UserService::Msg_Exception , $ex->getMessage() , $ex->getCode());
        }

    }

}
