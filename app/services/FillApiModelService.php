<?php

namespace App\services;


use App\enums\ActiveInactiveStatus;
use App\enums\ErrorCode;
use App\enums\OfferStatus;
use App\enums\RequestStatus;
use App\Models\API\auth\ApiResultProfileResult;
use App\Models\API\auth\ApiResultRegisterResult;
use App\Models\API\auth\ProfileResult;
use App\Models\API\auth\RegisterResult;
use App\Models\API\category\Category;
use App\Models\API\category\CategoryResultApi;
use App\Models\API\movie\Movie;
use App\Models\API\movie\MovieResultApi;
use App\Models\API\other\ApiMessage;
use App\Models\API\other\IdValueApiModel;



class FillApiModelService
{

    public static function FillProfileResultModel($item)
    {
        return new ProfileResult([
            'id' => $item->id,
            'name' => $item->name,
            'email' => $item->email,
        ]);

    }

    public static function FillApiResultProfileResult($item)
    {

        $model = new ApiResultProfileResult([
            'result' => $item,
            'isOk' => true,
            'message' => new ApiMessage([
                'type' => 'Success',
                'code' => ErrorCode::success,
                'content' => '',
            ])
        ]);

        return $model;
    }


    public static function FillCategoryApiModel($item)
    {
        return new Category([
            'id' => $item->id,
            'name' => $item->name,
            'status' => isset($item->status)
                ? self::FillIdValueApiModel($item->status, ActiveInactiveStatus::LabelOf($item->status))
                : $item->status,
        ]);
    }

    public static function FillCategoryResultApi($item)
    {

        $model = new CategoryResultApi([
            'result' => $item,
            'isOk' => true,
            'message' => new ApiMessage([
                'type' => 'Success',
                'code' => ErrorCode::success,
                'content' => '',
            ])
        ]);

        return $model;
    }


    public static function FillMovieApiModel($item)
    {
        return new Movie([
            'id' => $item->id,
            'name' => $item->name,
            'description' => $item->description,
            'image' => $item->image ? getImage($item->image) : null,
            'rate' => $item->rate,
            'status' => isset($item->status)
                ? self::FillIdValueApiModel($item->status, ActiveInactiveStatus::LabelOf($item->status))
                : $item->status,
        ]);
    }

    public static function FillMovieResultApi($item)
    {

        $model = new MovieResultApi([
            'result' => $item,
            'isOk' => true,
            'message' => new ApiMessage([
                'type' => 'Success',
                'code' => ErrorCode::success,
                'content' => '',
            ])
        ]);

        return $model;
    }


    public static function FillIdValueApiModel($id, $value)
    {
        $model = new IdValueApiModel([
            'id' => $id,
            'value' => $value ? $value : '',
        ]);

        return $model;
    }
}
