<?php

namespace App\services;

use App\enums\ErrorCode;
use App\enums\LoginApiEnum;
use App\enums\UserStatus;
use App\Models\API\auth\LoginResult;
use App\Models\API\auth\LoginResultApi;
use App\Models\API\other\ApiMessage;
use App\Models\API\other\ApiResult;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\services\FillApiModelService;

class UserService
{

    const Msg_RegistrationSuccess = "Registration Is Success !";
    const Msg_Exception = "Something is wrong !!";


    public static function apiProfileCreate($request)
    {
        $msg = [];
        $result = false;
        $model = null;
        $ex = '';
        $data = '';

        try {
            $model = new User([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'status' => UserStatus::STATUS_ACTIVE,
            ]);

            $result = $model->save();
            $token = Auth::guard('user-api')->attempt(['email' => $request->email, 'password' => $request->password]);

            $login_model = new LoginResult();

            $login_model->token = $token;
            $login_model->profile = FillApiModelService::FillProfileResultModel($model);

            $data = new LoginResultApi([
                'result' => $login_model,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

        } catch (\Exception $ex) {
            $ex = $ex->getMessage();
            $msg = AdminService::Msg_Exception;
        }

        return [$result, $data, $msg, $ex];
    }

    public static function profileUpdate($post)
    {
        $model = user();

        if ($model->load($post)) {

            if (!is_null(UploadedFile::getInstance($model, 'imagefile'))) {
                $model->imagefile = UploadedFile::getInstance($model, 'imagefile');
                $model->uploadImage($model) ? "" : $model->addError('imagefile', 'sth went wrong');
            }
            return [$model->save(), $model];
        } else {
            return [false, $model];
        }
    }

    public static function loginPhoneEmailValidation($request)
    {
        try {
            $login_model = new LoginResult();

            $token = Auth::guard('user-api')->attempt(['email' => $request->email, 'password' => $request->password]);
            $user = Auth::guard('user-api')->user();



            if (!$user) {
                return [false , null , LoginApiEnum::LabelOf(LoginApiEnum::not_found) , ''];
            }  elseif ($user->status == UserStatus::STATUS_INACTIVE) {
                return [false , null , LoginApiEnum::LabelOf(LoginApiEnum::not_active) , ''];
            } else {
                $login_model->token = $token;
                $login_model->profile = FillApiModelService::FillProfileResultModel($user);
            }



            $result = new LoginResultApi([
                'result' => $login_model,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserService::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function apiLogOut(Request $request)
    {
        try {
            $token = Str::after($request->header('Authorization'), 'Bearer ');
            JWTAuth::setToken($token)->invalidate();
            return returnSuccess("Logged out successfully");
        } catch (\Exception $ex) {
            return returnError(AdminService::Msg_Exception, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function checkToken()
    {
        if (!\user()) {
            return returnError(trans('Token expired'));
        }

        return returnSuccess(trans('Token Valid'));
    }
}
