<?php

namespace App\services;

use App\enums\ActiveInactiveStatus;
use App\enums\ErrorCode;
use App\enums\OfferStatus;
use App\enums\RequestStatus;
use App\enums\UserStatus;
use App\Models\API\other\ApiMessage;
use App\Models\Category;
use App\Models\CategoryMovie;
use App\Models\Movie;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\RedisQueue;


class MovieService {

    public static function getAll() {

        try {

            $query = Movie::get();

            $data = [];
            foreach ($query as $one) {

                $item = FillApiModelService::FillMovieApiModel($one);
                $data[] = $item;
            }

            return [true , FillApiModelService::FillMovieResultApi($data) , '' , ''];
        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }
    }


    public static function getOne($request) {

        try {

            $movie = Movie::find($request->movie_id);

            if(!$movie){
                return [false , null , "This movie is not found !" , ''];
            }

            $data = FillApiModelService::FillMovieApiModel($movie);

            return [true , FillApiModelService::FillMovieResultApi($data) , '' , ''];
        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }
    }

    public static function createUpdateMovie($request){
        try{

            $movie = (isset($request->movie_id)) ? Movie::find($request->movie_id) : new Movie();
            if(!$movie){
                return [false , null , "This movie is not found !" , ''];
            }

            $movie->name = isset($request->name) ? $request->name : $movie->name;
            $movie->description = isset($request->description) ? $request->description : $movie->description;
            $movie->rate = isset($request->rate) ? $request->rate : $movie->rate;
            $movie->status = isset($request->status) ? $request->status : ActiveInactiveStatus::active;

            if ($request->image) {
                $movie->image = uploadImage($request->image, Movie::image_directory);
            }

            $movie->save();
            $data = FillApiModelService::FillMovieApiModel($movie);
            $data = FillApiModelService::FillMovieResultApi($data);
            return [true , $data , '' , ''];

        }catch (\Exception $ex){
            return [false , null , UserService::Msg_Exception , $ex->getMessage()];
        }

    }

    public static function delete($request){

        try {
            $movie = Movie::find($request->movie_id);

            if (!$movie)
                return returnError("This movie is not found !");

            $movie->delete();
            return returnSuccess("This movie has been deleted");
        }catch (\Exception $ex){
            return returnError(UserService::Msg_Exception , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function create_associate($request){

        try {
            $movie = Movie::find($request->movie_id);

            if (!$movie)
                return returnError("This movie is not found !");

            $category = Category::find($request->category_id);

            if (!$category)
                return returnError("This category is not found !");

            $category_movies = new CategoryMovie();
            $category_movies->category_id = $request->category_id;
            $category_movies->movie_id = $request->movie_id;
            $category_movies->save();

            return returnSuccess("This movie has been associated !!");
        }catch (\Exception $ex){
            return returnError(UserService::Msg_Exception , $ex->getMessage() , $ex->getCode());
        }

    }

    public static function delete_associate($request){

        try {
            $movie = Movie::find($request->movie_id);

            if (!$movie)
                return returnError("This movie is not found !");

            $category = Category::find($request->category_id);

            if (!$category)
                return returnError("This category is not found !");

            $category_movies = CategoryMovie::
                where('category_id' , $request->category_id)
                ->where('movie_id' , $request->movie_id)
                ->first();

            if (!$category_movies)
                return returnError("This associate is not found !");

            $category_movies->delete();

            return returnSuccess("This movie has been associated !!");
        }catch (\Exception $ex){
            return returnError(UserService::Msg_Exception , $ex->getMessage() , $ex->getCode());
        }

    }

}
