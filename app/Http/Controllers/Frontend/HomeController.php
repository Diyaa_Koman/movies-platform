<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryMovie;
use App\Models\Movie;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $movies = Movie::orderBy('created_at' , 'desc');

        if(\request()->search_field){
            $term = request()->search_field;
            $movies = $movies->where('name' , 'Like' , "%{$term}%");
        }

        $categories_select = null;

        if(\request()->category) {

            $category_ids = \request()->category;
            $category_movies = CategoryMovie::query()
                ->whereIn('category_id', $category_ids)
                ->select('movie_id')
                ->get();

            $categories_select = Category::whereIn('id' , $category_ids)->get();

            $movies = $movies->whereIn('id', $category_movies);
        }


        $movies = $movies->get();
        $categories = Category::get();

        return view('index' , compact('movies' , 'categories' , 'categories_select'));
    }

    public function show_movie($id){

        $movie = Movie::find($id);

        $category_movies = CategoryMovie::where('movie_id', $id)
            ->select('category_id')
            ->get();

        $categories = Category::whereIn('id', $category_movies)->get();
        return view('movie', compact('movie' , 'categories'));
    }


}
