<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Movie\AssociateRequest;
use App\Http\Requests\Movie\CreateRequest;
use App\Http\Requests\Movie\GetRequest;
use App\Http\Requests\Movie\UpdateRequest;
use App\services\MovieService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthController
 */

class MovieController extends Controller
{

    /**
     * @OA\Get(path="/movie/get-all",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Get All Categories",
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/MovieResultApi"),
     *     ),
     * )
     */

    public function getAll()
    {
        list($res, $data, $msg , $ex) = MovieService::getAll();

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }


    /**
     * @OA\Get(path="/movie/get-one",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Get One Categories",
     *     @OA\Parameter(
     *         name="movie_id",
     *         description="id of movie",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/MovieResultApi"),
     *     ),
     * )
     */

    public function getOne(GetRequest $request)
    {
        list($res, $data, $msg , $ex) = MovieService::getOne($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }


    /**
     * @OA\Post(path="/movie/create",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Create a new movie",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *              @OA\Schema(ref="#components/schemas/CreateUpdateMovieModel"),
     *        )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/MovieResultApi"),
     *     ),
     * )
     */

    public function create(CreateRequest $request)
    {
        list($res, $data, $msg , $ex) = MovieService::createUpdateMovie($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Post(path="/movie/update",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Update a movie",
     *     @OA\Parameter(
     *         name="movie_id",
     *         description="id of movie",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *              @OA\Schema(ref="#components/schemas/CreateUpdateMovieModel"),
     *        )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "MovieResultApi response",
     *         @OA\JsonContent(ref="#/components/schemas/MovieResultApi"),
     *     ),
     * )
     */

    public function update(UpdateRequest $request)
    {
        list($res, $data, $msg , $ex) = MovieService::createUpdateMovie($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Delete(path="/movie/delete",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Delete a movie",
     *     @OA\Parameter(
     *         name="movie_id",
     *         description="id of movie",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function delete(GetRequest $request)
    {
        return MovieService::delete($request);
    }

    /**
     * @OA\Post(path="/movie/create-associate",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Associate a movie with category",
     *     @OA\Parameter(
     *         name="movie_id",
     *         description="id of movie",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="category_id",
     *         description="id of category",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function create_associate(AssociateRequest $request)
    {
        return MovieService::create_associate($request);
    }

    /**
     * @OA\Delete(path="/movie/delete-associate",
     *     tags={"Movie"},
     *     security={{"apiAuth":{}}},
     *     summary="Delete associate a movie with category",
     *     @OA\Parameter(
     *         name="movie_id",
     *         description="id of movie",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="category_id",
     *         description="id of category",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function delete_associate(AssociateRequest $request)
    {
        return MovieService::delete_associate($request);
    }

}
