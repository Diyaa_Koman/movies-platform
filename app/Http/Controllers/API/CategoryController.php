<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CreateRequest;
use App\Http\Requests\Category\GetRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\services\CategoryService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthController
 */

class CategoryController extends Controller
{

    /**
     * @OA\Get(path="/category/get-all",
     *     tags={"Category"},
     *     security={{"apiAuth":{}}},
     *     summary="Get All Categories",
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/CategoryResultApi"),
     *     ),
     * )
     */

    public function getAll()
    {
        list($res, $data, $msg , $ex) = CategoryService::getAll();

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }


    /**
     * @OA\Get(path="/category/get-one",
     *     tags={"Category"},
     *     security={{"apiAuth":{}}},
     *     summary="Get One Categories",
     *     @OA\Parameter(
     *         name="category_id",
     *         description="id of category",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/CategoryResultApi"),
     *     ),
     * )
     */

    public function getOne(GetRequest $request)
    {
        list($res, $data, $msg , $ex) = CategoryService::getOne($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }


    /**
     * @OA\Post(path="/category/create",
     *     tags={"Category"},
     *     security={{"apiAuth":{}}},
     *     summary="Create a new category",
     *     @OA\RequestBody(
     *         description="Create Category Model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateUpdateCategoryModel")
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/CategoryResultApi"),
     *     ),
     * )
     */

    public function create(CreateRequest $request)
    {
        list($res, $data, $msg , $ex) = CategoryService::createUpdateCategory($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Put(path="/category/update",
     *     tags={"Category"},
     *     security={{"apiAuth":{}}},
     *     summary="Update a category",
     *     @OA\Parameter(
     *         name="category_id",
     *         description="id of category",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Update Category Model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateUpdateCategoryModel")
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/CategoryResultApi"),
     *     ),
     * )
     */

    public function update(UpdateRequest $request)
    {
        list($res, $data, $msg , $ex) = CategoryService::createUpdateCategory($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Delete(path="/category/delete",
     *     tags={"Category"},
     *     security={{"apiAuth":{}}},
     *     summary="Delete a category",
     *     @OA\Parameter(
     *         name="category_id",
     *         description="id of category",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function delete(GetRequest $request)
    {
        return CategoryService::delete($request);
    }

}
