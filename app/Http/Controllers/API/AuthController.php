<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePhoneRequest;
use App\Http\Requests\Auth\LocationRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\VerifyAccountRequest;
use App\Models\API\auth\RegisterResult;
use App\services\FillApiModelService;
use App\services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class AuthController
 */

class AuthController extends Controller
{

    /**
     * @OA\Post(path="/register",
     *     tags={"Auth"},
     *     summary="Register as a new user",
     *     operationId="authRegister",
     *     @OA\RequestBody(
     *         description="Registration model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RegisterModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/LoginResultApi"),
     *     ),
     * )
     * @param RegisterRequest $request
     * @return string
     */

    public function register(RegisterRequest $request)
    {
        list($res, $data, $msg , $ex) = UserService::apiProfileCreate($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }

    /**
     * @OA\Post(path="/login",
     *     tags={"Auth"},
     *     summary="Login as a user",
     *     operationId="authLogin",
     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginPayload")
     *     ),
     *     @OA\Response(
     *         response = 202,
     *         description = "User Profile response",
     *         @OA\JsonContent(ref="#/components/schemas/LoginResultApi"),
     *     ),
     * )
     * @param LoginRequest $request
     * @return JsonResponse
     */

    public function login(LoginRequest $request)
    {
        list($res, $data, $msg , $ex) = UserService::loginPhoneEmailValidation($request);

        if($res){
            return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
        } else {
            return returnError($msg , $ex);
        }
    }


    /**
     * @OA\Post(path="/logout",
     *     tags={"Auth"},
     *     summary="Log out from system",
     *     security={{"apiAuth":{}}},
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param $request
     * @return JsonResponse
     */

    public function logout(Request $request)
    {
        return UserService::apiLogOut($request);
    }


    /**
     * @OA\Get(path="/profile",
     *     tags={"Auth"},
     *     summary="Get profile details",
     *     security={{"apiAuth":{}}},
     *     @OA\Response(
     *         response = 202,
     *         description = "ApiResultProfileResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResultProfileResult"),
     *     ),
     * )
     */

    public function profile()
    {
        $data = FillApiModelService::FillProfileResultModel(user());
        $data = FillApiModelService::FillApiResultProfileResult($data);
        return response()->json($data)->setStatusCode(Response::HTTP_ACCEPTED);
    }

}
