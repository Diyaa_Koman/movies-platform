<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\category;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateUpdateCategoryModel model",
 *     description="CreateUpdateCategoryModel model",
 * )
 */

class CreateUpdateCategoryModel extends Model
{

   /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

}

