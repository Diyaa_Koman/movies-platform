<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\category;
use App\Models\API\other\IdValueApiModel;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateCategoryModel model",
 *     description="CreateCategoryModel model",
 * )
 */

class Category extends Model
{
    protected $fillable = [
        'id' , 'name' , 'status'
    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var string
     */
    public $id;

   /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;


    /**
     * @OA\Property(
     *     description="Status",
     *     title="status",
     * )
     *
     * @var IdValueApiModel
     */
    public $status;
}

