<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\movie;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateUpdateCategoryModel model",
 *     description="CreateUpdateCategoryModel model",
 * )
 */

class CreateUpdateMovieModel extends Model
{

   /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     description="Description",
     *     title="description",
     * )
     *
     * @var string
     */
    public $description;



    /**
     * @OA\Property(
     *  description="User image",
     *  property="image",
     *  type="string",
     *  format="binary"
     * )
     *
     */

    public $image;

    /**
     * @OA\Property(
     *     description="Rate",
     *     title="rate",
     * )
     *
     * @var integer
     */
    public $rate;


}

