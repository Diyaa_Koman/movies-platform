<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\movie;
use App\Models\API\other\IdValueApiModel;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateCategoryModel model",
 *     description="CreateCategoryModel model",
 * )
 */

class Movie extends Model
{
    protected $fillable = [
        'id' , 'name' , 'description' ,
        'image' , 'rate' ,'status'
    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var string
     */
    public $id;

   /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;


    /**
     * @OA\Property(
     *     description="Description",
     *     title="description",
     * )
     *
     * @var string
     */
    public $description;



    /**
     * @OA\Property(
     *     description="Image",
     *     title="image",
     * )
     *
     * @var string
     */
    public $image;

    /**
     * @OA\Property(
     *     description="Rate",
     *     title="rate",
     * )
     *
     * @var integer
     */
    public $rate;


    /**
     * @OA\Property(
     *     description="Status",
     *     title="status",
     * )
     *
     * @var IdValueApiModel
     */
    public $status;
}

