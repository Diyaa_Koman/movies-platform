<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\other;


use App\Models\API\other\ApiMessage;
use Illuminate\Database\Eloquent\Model;


class ApiDataResult extends Model
{
    protected $fillable = ['result' , 'isOk' , 'message' , 'exception'];

    /**
     * @var object
     */
    public $result;


    /**
     * @var boolean
     */
    public $isOk;

    /**
     * @var ApiMessage
     */
    public $message;

    /**
     * @var string
     */
    public $exception;

}

