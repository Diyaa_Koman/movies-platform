<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\API\auth;


use Illuminate\Database\Eloquent\Model;

/**
 * Class LoginResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="LoginResult model",
 *     description="LoginResult model",
 * )
 */
class LoginResult
{

    /**
     * @OA\Property(
     *     description="Access token",
     *     title="token",
     * )
     *
     * @var string
     */
    public $token;

    /**
     * @OA\Property(
     *     description="Profile data",
     *     title="profile",
     * )
     *
     * @var ProfileResult
     */
    public $profile;

}

