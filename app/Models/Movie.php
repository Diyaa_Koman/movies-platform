<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    const image_directory = 'movies';

    protected $fillable = [
        'name' , 'description' , 'image' , 'rate' ,'status'
    ];
}
